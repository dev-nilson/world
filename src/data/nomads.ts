export const nomads = [
  {
    id: 1,
    name: "Denilson Lemus",
    description: "Software Developer",
    long: -89.2182,
    lat: 13.6929,
    bio: "Made in El Salvador",
    links: [
      {
        social: "facebook",
        url: "https://www.facebook.com/denilfredo",
      },
      {
        social: "twitter",
        url: "https://www.twitter.com/devnilson",
      },
      {
        social: "instagram",
        url: "https://www.instagram.com/denilemus",
      },
    ],
  },
];
