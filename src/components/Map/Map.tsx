"use client";
import { useContext, useState } from "react";
import ReactMapGL from "react-map-gl";
import { ThemeContext } from "@/contexts/ThemeContext";
import { View } from "@/types/interfaces";
import { ThemeContextType } from "@/app/page";
import { nomads } from "@/data/nomads";
import Pin from "../Pin/Pin";
import "mapbox-gl/dist/mapbox-gl.css";

function Map() {
  const { theme } = useContext(ThemeContext) as ThemeContextType;
  const [viewport, setViewport] = useState<View>({
    width: "100%",
    height: "100%",
    longitude: -89.2182,
    latitude: 13.6929,
    zoom: 2,
  });

  return (
    <ReactMapGL
      projection={"globe"}
      mapStyle={
        theme === "light"
          ? process.env.NEXT_PUBLIC_MAPBOX_LIGHT_STYLE
          : process.env.NEXT_PUBLIC_MAPBOX_DARK_STYLE
      }
      mapboxAccessToken={process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN}
      onMove={(e) => setViewport(e.viewState)}
      {...viewport}
    >
      {nomads.map((item) => (
        <Pin
          key={item.id}
          name={item.name}
          description={item.description}
          bio={item.bio}
          long={item.long}
          lat={item.lat}
          links={item.links}
        />
      ))}
    </ReactMapGL>
  );
}

export default Map;
