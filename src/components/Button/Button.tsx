import { useContext } from "react";
import { ThemeContextType } from "@/app/page";
import { ThemeContext } from "@/contexts/ThemeContext";
import styles from "./Button.module.css";

export type ButtonProps = {
  label: string;
  onClick: () => void;
};

function Button({ label, onClick }: ButtonProps) {
  const { theme } = useContext(ThemeContext) as ThemeContextType;

  return (
    <button className={`${styles.button} ${styles[theme]}`} onClick={onClick}>
      {label}
    </button>
  );
}

export default Button;
