import { useContext } from "react";
import { ThemeContextType } from "@/app/page";
import { ThemeContext } from "@/contexts/ThemeContext";
import styles from "./Modal.module.css";

type ModalProps = {
  onClick: () => void;
};

function Modal({ onClick }: ModalProps) {
  const { theme } = useContext(ThemeContext) as ThemeContextType;

  return (
    <div className={`${styles.modal} ${styles[theme]}`}>
      <div className={styles.close} onClick={onClick}>
        ❌
      </div>
      <div className={styles.modal__body}>
        <h1 className={styles.icon}>✋</h1>
        <h3>Want to add your location?</h3>
        <p>Send me a message</p>
        <div>
          <a href="https://www.facebook.com/Denilfredo" className={styles.facebook}>Facebook</a>
          <a href="https://twitter.com/devnilson" className={styles.twitter}>Twitter</a>
          <a href="https://www.instagram.com/denilemus" className={styles.instagram}>Instagram</a>
        </div>
      </div>
    </div>
  );
}

export default Modal;
