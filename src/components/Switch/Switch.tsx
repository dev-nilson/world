import { useContext } from "react";
import { ThemeContextType } from "@/app/page";
import { ThemeContext } from "@/contexts/ThemeContext";
import styles from "./Switch.module.css";

function Switch() {
  const { theme, toggleTheme } = useContext(ThemeContext) as ThemeContextType;

  return (
    <label className={`${styles.switch} ${styles[theme]}`}>
      <input type="checkbox" onChange={toggleTheme} />
      <span className={styles.slider} />
    </label>
  );
}

export default Switch;
