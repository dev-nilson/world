import { useContext } from "react";
import { ThemeContextType } from "@/app/page";
import { ThemeContext } from "@/contexts/ThemeContext";
import Switch from "../Switch/Switch";
import styles from "./Navbar.module.css";

function Navbar() {
  const { theme } = useContext(ThemeContext) as ThemeContextType;

  return (
    <nav className={styles.navbar}>
      <div>
        <h3 className={`${styles.logo} ${styles[theme]}`}>The Nomadic 🌎</h3>
      </div>

      <div className={styles.controls}>
        <span>🌑</span>
        <Switch />
        <span>☀️</span>
      </div>
    </nav>
  );
}

export default Navbar;
