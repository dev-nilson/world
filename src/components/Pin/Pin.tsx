import React, { useState } from "react";
import { Marker, Popup } from "react-map-gl";
import { FaFacebook, FaTwitter, FaInstagram } from "react-icons/fa";
import Image from "next/image";
import image from "@/assets/image.png";
import styles from "./Pin.module.css";

function Pin({ name, description, lat, long, bio, links }: any) {
  const [showPopup, setShowPopup] = useState(false);

  return (
    <>
      <Marker longitude={long} latitude={lat}>
        <span className="marker" onClick={() => setShowPopup((prev) => !prev)}>
          🟢
        </span>
      </Marker>
      {showPopup && (
        <Popup
          className={styles.popup}
          longitude={long}
          latitude={lat}
          anchor="top"
          offset={6}
          focusAfterOpen={false}
          closeOnMove={true}
          closeButton={false}
          closeOnClick={false}
          onClose={() => {
            setShowPopup(false);
          }}
        >
          <div className={styles.popup}>
            <div className={styles.popup__head}>
              <Image
                className={styles.image}
                src={image}
                alt="profile"
                width={50}
                height={50}
              />
              <div>
                <h2>{name}</h2>
                <p>{description}</p>
              </div>
            </div>
            <div className={styles.popup__body}>
              <p>{bio}</p>
            </div>
            <div className={styles.popup__footer}>
              {links.map((link: any) => (
                <a href={link.url} key={link.social}>
                  {link.social === "facebook" && (
                    <FaFacebook className={styles.link} />
                  )}
                  {link.social === "instagram" && (
                    <FaInstagram className={styles.link} />
                  )}
                  {link.social === "twitter" && (
                    <FaTwitter className={styles.link} />
                  )}
                </a>
              ))}
            </div>
          </div>
        </Popup>
      )}
    </>
  );
}

export default React.memo(Pin);
