export default function Head() {
  return (
    <>
      <title>The Nomadic World</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="The Nomadic World" content="Nomads all over the world" />
      <link rel="icon" href="/favicon.png" />
    </>
  )
}
