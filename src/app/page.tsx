"use client";
import React, { useState, createContext } from "react";
import { Inter } from "@next/font/google";
import { ThemeContext } from "@/contexts/ThemeContext";
import Navbar from "@/components/Navbar/Navbar";
import Map from "@/components/Map/Map";
import Button from "@/components/Button/Button";
import Modal from "@/components/Modal/Modal";
import styles from "./page.module.css";

const inter = Inter({ subsets: ["latin"] });

export type ThemeContextType = {
  theme: "light" | "dark";
  toggleTheme: () => void;
};

export default function Home() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [theme, setTheme] = useState<"light" | "dark">("dark");

  const toggleTheme = () => {
    setTheme((prev) => (prev === "light" ? "dark" : "light"));
  };

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      <main className={styles.app}>
        <Navbar />
        <Map />
        <Button label="📍" onClick={() => setIsModalOpen(true)} />
        {isModalOpen && <Modal onClick={() => setIsModalOpen(false)} />}
      </main>
    </ThemeContext.Provider>
  );
}
