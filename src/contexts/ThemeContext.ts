import { ThemeContextType } from "@/app/page";
import { createContext } from "react";

export const ThemeContext = createContext<ThemeContextType | null>(null);
